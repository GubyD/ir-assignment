<%--
  Created by IntelliJ IDEA.
  User: XuanWei
  Date: 29/3/2017
  Time: 2:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>TechGeeks Portal</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- for scaling on mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        //        var URL_Prefix = "http://localhost:8983/solr/techgeek/suggest";
        //        var URL_Suffix = { wt: "json", sort: "true" };
        var URL_Prefix = "http://localhost:8983/solr/techgeek/suggest?q=";
        var URL_Suffix = "&wt=json&sort=true";

        $(function() {

            $("#search-bar").keypress(function(e) {
                if (e.which == 13) {
                    searchRedirect();
                }
            });

            function searchRedirect() {
                var keyword = $("#search-bar").val();

                if (keyword.length != 0) {
                    window.location.href='results.jsp?q='+keyword;
                }
            }


            $("#search-bar").autocomplete({
                source: function(request, response) {
//                    URL_Suffix.q = $('#search-bar').val().replace(" ", "+");
                    var URL = URL_Prefix + $('#search-bar').val().split(' ').join('+') + URL_Suffix;

                    $.ajax({
                        url: URL,
                        async: false,
                        success: function (data) {
                            if (data.spellcheck.hasOwnProperty('collations')) {
                                var suggestions = JSON.stringify(data.spellcheck.collations);
                                var jsonData = JSON.parse(suggestions);
                                if(jsonData.length!=0){
                                    for( var i = jsonData.length-1; i--;){
                                        if ( jsonData[i] === 'collation') jsonData.splice(i, 1);
                                    }
                                }
                            }
                            response($.map(jsonData, function (value, key) {
                                return {
                                    label: value
                                }
                            }));
                        },
                        dataType: 'jsonp',
                        jsonp: 'json.wrf'
                    });
                },
                minLength:1
            });

            $("#button_build_classifier").click(function(){
                var $this = $(this);
                $this.html("<i class='fa fa-circle-o-notch fa-spin'></i> Building");
                $this.prop("disabled", true);

                $.ajax({
                    url: "http://localhost:8080/Classification",
                    dataType: "text",
                    success: function (result) {
                        $this.prop("disabled", false);
                        $this.html("Build Classifier");
                    }
                });
            });

            $("#button_crawl").click(function(){
                var $this = $(this);
                $this.html("<i class='fa fa-circle-o-notch fa-spin'></i> Updating");
                $this.prop("disabled", true);

                $.ajax({
                    url: "http://localhost:8080/Crawl",
                    dataType: "text",
                    success: function (result) {
                        $this.prop("disabled", false);
                        $this.html("Update Data");
                    }
                });
            });
        });
    </script>
</head>
<body>
<div class="full-container1">
    <div class="row">
        <div class="col-md-12">
            <div class="header_promo">
                <h1 style="font-family:Montserrat; font-weight: bolder;">T<span style="font-size:72%">ECH</span><span style="margin-left: 3px">G<span style="font-size:72%">EEKS</span></span></h1>
                <div class="pull-right" style="margin-top: -50px; margin-right: 150px;">
                    <button id="button_build_classifier" type="button" class="btn btn-danger btn-lg" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Building">Build Classifier</button>
                    <button id="button_crawl" type="button" class="btn btn-primary btn-lg">Update Data</button>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="full-container">
    <div class="start_description">

        <center>
            <h1 class="display-inline" style="padding-left:0">WELCOME TO TECHGEEKS PORTAL!</h1>
            <p class="margin-top-sm margin-bottom-md">Read the news on the latest technology around the world!</p>
        </center>

        <div class="search_promo">
            <div class="input-group">
                <input id="search-bar" type="text" class="form-control" placeholder="Search for . . .">
                <div class="input-group-btn btn_promo_search">
                    <button type="button" class="btn btnsearch" onclick="searchRedirect();" style="color:#fff">Search</button>
                </div>
            </div>
        </div>
    </div>
</div><!-- full-container -->
</body>
</html>
