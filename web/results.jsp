<%--
  Created by IntelliJ IDEA.
  User: XuanWei
  Date: 29/3/2017
  Time: 2:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>TechGeeks Portal</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- for scaling on mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="js/jquery.twbsPagination.js"></script>

    <style>
        .bg-info {
            color: #e73931;
            background: #d9edf7;
        }
    </style>
    <script>
        var first_time = true;
        var keyword = "";
        var params = {
            "facet.field": "source",
            bf: "recip(abs(ms(NOW,created_time)),3.16e-11,1,1) scale(likes,0,1)",
            defType: "edismax",
            wt: "json",
            facet: "on"
        };

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        $(function() {
            keyword = getUrlParameter('q');

            if (keyword != null && keyword.length != 0) {
                $("#search-bar").val(keyword);

                first_time = true;
                search(params, 0);
            } else {
                window.location.href='index.jsp';
            }

            $("#search-bar").keypress(function(e) {
                if(e.which == 13) {
                    keyword = $(this).val();

                    first_time = true;
                    search(params, 0);
                }
            });

            $('input[type=radio][name=filter]').change(function() {
                first_time = true;
                search(params, 0);
            });

            $('.category').click(function() {
                var self = $(this);

                if (self.hasClass('active')) {
                    self.removeClass('active');
                } else {
                    self.addClass('active');
                }

                updateFqParam();

                first_time = true;
                search(params, 0);
            });

            $('.source').click(function() {
                var self = $(this);
                $('.source').each(function() {
                    var other = $(this);
                    if (!(self.attr('id') === other.attr('id')) && other.hasClass('active')) {
                        other.removeClass('active');
                    }
                });

                if (self.hasClass('active')) {
                    self.removeClass('active');
                } else {
                    self.addClass('active');
                }

                updateFqParam();

                first_time = true;
                search(params, 0);
            });

            /**
             * Autocomplete
             * **/
            var URL_Prefix = "http://localhost:8983/solr/techgeek/suggest?q=";
            var URL_Suffix = "&wt=json&sort=true";

            $("#search-bar").autocomplete({
                source: function(request, response) {
//                    URL_Suffix.q = $('#search-bar').val().replace(" ", "+");
                    var URL = URL_Prefix + $('#search-bar').val().split(' ').join('+') + URL_Suffix;

                    $.ajax({
                        url: URL,
                        async: false,
                        success: function (data) {
                            if (data.spellcheck.hasOwnProperty('collations')) {
                                var suggestions = JSON.stringify(data.spellcheck.collations);
                                var jsonData = JSON.parse(suggestions);
                                if(jsonData.length!=0){
                                    for( var i = jsonData.length-1; i--;){
                                        if ( jsonData[i] === 'collation') jsonData.splice(i, 1);
                                    }
                                }
                            }
                            response($.map(jsonData, function (value, key) {
                                return {
                                    label: value
                                }
                            }));
                        },
                        dataType: 'jsonp',
                        jsonp: 'json.wrf'
                    });
                },
                minLength:1
            });
        });

        function search(data, start) {
            data.bf = $('input[type=radio][name=filter]:checked').val();
            data.q  = keyword;
            data.start = start;

            $.ajax({
                url: "http://localhost:8983/solr/techgeek/select",
                data: data,
                dataType: "jsonp",
                jsonp: 'json.wrf',
                success: function (result) {
                    var header, response, facet_counts;

                    header = result.responseHeader;

                    if (header.status == 0) {

                        response = result.response;
                        facet_counts = result.facet_counts.facet_fields.source;

                        $('#result-cards').empty();

                        if (response.docs.length != 0) {
                            var numFound = (response.numFound+'').replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            $('#result-header').text('Result for '+ keyword);
                            $('#result-info').html('About <b>'+ numFound +'</b> results ('+ (header.QTime/1000) +' seconds)');

                            $.each(response.docs, function(i, doc){
                                displayResult(doc);
                            });

                            if (start == 0 && first_time) {
                                first_time = false;
                                updatePagination(response);
                                updateFacetCounts(facet_counts);
                            } else {
                                $('html,body').animate({
                                        scrollTop: 0
                                    },
                                    'slow');
                            }
                        } else {
                            suggestion();
                        }
                    }
                }
            });
        }

        function displayResult(doc) {
            var source_link = getSourceLink(doc.source.toLowerCase());

            var result_card = $('<div class="search-result"> ' +
                '   <div class="row">' +
                '       <div class="col-md-5"> ' +
                '           <a href="'+ doc.link +'"><img style="border: 1px solid #d2d2d2;" src="'+ doc.full_picture +'" class="img-responsive center-block margin-bottom-sm"/></a>' +
                '       </div>' +
                '       <div class="col-md-7">' +
                '           <h3 class="display-inline">' +
                '               <a href="'+ doc.link +'">'+ doc.name +'</a>' +
                '           </h3>' +
                '           <br/>' +
                '           <div class="margin-top-sm">' +
                '               <img src="img/'+ doc.source.toLowerCase() +'.jpg" class="img-responsive img-circle display-inline margin-right-sm navbar-left" width="40">' +
                '               <span class="display-inline" style="vertical-align: top">' +
                '                   <a href="'+ source_link +'">'+ doc.source.toUpperCase() +'</a> | '+ doc.category +
                '                   <br />' +
                '                   <div style="font-size: smaller; color: #56585c; margin-top: 2px;">' +
                '                       <span class="glyphicon glyphicon-time"></span> '+ timeSince(Date.parse(doc.created_time)) +' ago <span style="font-weight:bolder; font-size: larger;">.</span> <img src="img/icon_like.png" width="12" style="margin-bottom: 2px"> '+ doc.likes +
                '                   </div>' +
                '               </span>' +
                '           </div>' +
                '           <div class="margin-bottom-sm margin-top-sm">'+ doc.description +'</div>' +
                '       </div><!-- ./row -->' +
                '   </div>' +
                '</div>');

            $('#result-cards').append(result_card);
        }

        function getSourceLink(source) {
            var link = "http://www.pcworld.com/";
            switch (source) {
                case "t3mag":
                    link = "http://www.t3.com/";
                    break;
                case "engadget":
                    link = "https://www.engadget.com/";
                    break;
                case "techradar":
                    link = "http://www.techradar.com/";
                    break;
                case "techcrunch":
                    link = "https://techcrunch.com/";
                    break;
            }

            return link;
        }

        function updateFacetCounts(sources) {
            var source, counts = 0;
            for (i = 0; i < sources.length; i+=2) {
                source = sources[i].toLowerCase();
                counts = sources[i+1];
                if (counts != 0) {
                    $('#badge-'+source).text(counts);
                } else {
                    $('#badge-'+source).text('');
                }
            }
        }

        function updateFqParam() {
            var categories = $('.category.active')
                .map(function () {
                    return $(this).attr('category').trim();
                }).get();

            var source = $('.source.active')
                .map(function () {
                    return $(this).attr('id').trim();
                }).get();


            var fq = '';

            if (source.length != 0)
                fq = 'source:'+ source[0];

            if (categories.length != 0) {
                if (fq.length != 0) fq += ' AND ';

                fq += 'category:('+ categories.join(' OR ') +')';
            }

            if (fq.length == 0) {
                delete params['fq'];
            } else {
                params.fq = fq;
            }
        }

        function timeSince(date) {

            var seconds = Math.floor((new Date() - date) / 1000);

            var interval = Math.floor(seconds / 31536000);

            if (interval > 1) {
                return interval + " years";
            }
            interval = Math.floor(seconds / 2592000);
            if (interval > 1) {
                return interval + " months";
            }
            interval = Math.floor(seconds / 86400);
            if (interval > 1) {
                return interval + " days";
            }
            interval = Math.floor(seconds / 3600);
            if (interval > 1) {
                return interval + " hours";
            }
            interval = Math.floor(seconds / 60);
            if (interval > 1) {
                return interval + " minutes";
            }
            return Math.floor(seconds) + " seconds";
        }

        function updatePagination(response) {
            $('#pagination').twbsPagination('destroy');
            $('#pagination').twbsPagination({
                totalPages: Math.ceil(response.numFound/10),
                visiblePages: 5,
                cssStyle: '',
                prevText: '<span aria-hidden="true">&laquo;</span>',
                nextText: '<span aria-hidden="true">&raquo;</span>',
                onPageClick: function (event, page) {
                    search(params, (page-1)*10);
                }
            });
        }

        function suggestion() {
            $.ajax({
                url: "http://localhost:8983/solr/techgeek/spell",
                data: {
                    wt: "json",
                    q: keyword
                },
                dataType: "jsonp",
                jsonp: 'json.wrf',
                success: function (result) {
                    var header, spellchecks;

                    header = result.responseHeader;
                    spellchecks = result.spellcheck.suggestions;

                    if (header.status == 0) {
                        var suggest_words = "";
                        for (i = 0; i < spellchecks.length; i+=2) {
                            var suggestion = spellchecks[i+1].suggestion;

                            if (suggestion.length != 0) {
                                suggest_words += (suggestion[0]).word+" ";
                            }
                        }

                        suggest_words = suggest_words.trim();

                        $('#result-info').html('<b>No result</b> found for '+ keyword);
                        $('#result-header').html('Do you mean <a href="results.jsp?q='+suggest_words+'" onclick="" style="text-decoration: underline">'+suggest_words+'</a>?');

                        $('#badge-engadget, #badge-techradar, #badge-pcworld, #badge-t3mag, #badge-techcrunch').text('');
                        $('#pagination').twbsPagination('destroy');
                    }
                }
            });
        }
    </script>
</head>
<body>
<div class="results-container1">
    <div class="row">
        <div class="col-sm-12 text-center">
            <div class="col-sm-4">
                <div class="header_promo2" style="margin-top:-10px;">
                    <h1 style="font-family:Montserrat; font-weight: bolder;">T<span style="font-size:72%">ECH</span><span style="margin-left: 3px">G<span style="font-size:72%">EEKS</span></span></h1>
                </div>
            </div>
            <div class="col-sm-8">
                <div style="padding-bottom:20px;" >
                    <div role="search" class="search navbar-right">
                        <div class="form-group">
                            <input id="search-bar" type="text" class="form-control" placeholder="Search">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container contant">
    <div class="row cont">
        <div class="col-md-3 sidebar">

            <div class="affix-top" id="myAffix" data-spy="affix" data-offset-top="30" data-offset-bottom="20">
                <ul class="sources">
                    <li>
                        <a href="#" id="Engadget" class="source">
                            <img src="img/engadget.jpg" class="img-responsive img-circle display-inline margin-right-sm navbar-left" style="border: 1px solid #342C43;" width="20">
                            Engadget<span id="badge-engadget" class="badge bg-info pull-right"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="PCWorld" class="source">
                            <img src="img/pcworld.jpg" class="img-responsive img-circle display-inline margin-right-sm navbar-left" style="border: 1px solid #342C43;" width="20">
                            PCWorld<span id="badge-pcworld" class="badge bg-info pull-right"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="TechRadar" class="source">
                            <img src="img/techradar.jpg" class="img-responsive img-circle display-inline margin-right-sm navbar-left" style="border: 1px solid #342C43;" width="20">
                            TechRadar<span id="badge-techradar" class="badge bg-info pull-right"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="t3mag" class="source">
                            <img src="img/t3mag.jpg" class="img-responsive img-circle display-inline margin-right-sm navbar-left" style="border: 1px solid #342C43;" width="20">
                            T3<span id="badge-t3mag" class="badge bg-info pull-right"></span>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="techcrunch" class="source">
                            <img src="img/techcrunch.jpg" class="img-responsive img-circle display-inline margin-right-sm navbar-left" style="border: 1px solid #342C43;" width="20">
                            TechCrunch<span id="badge-techcrunch" class="badge bg-info pull-right"></span>
                        </a>
                    </li>
                </ul>
                <div class="categories">
                    <h3>Category</h3>
                    <ul>
                        <li>
                            <a href="#" category="Mobile" class="category">
                                <i class="glyphicon glyphicon-phone"></i> Mobile
                            </a>
                        </li>
                        <li>
                            <a href="#" category="Gaming" class="category">
                                <i class="fa fa-gamepad"></i> Gaming
                            </a>
                        </li>
                        <li>
                            <a href="#" category="Computing" class="category">
                                <i class="fa fa-cloud"></i> Computing
                            </a>
                        </li>
                        <li>
                            <a href="#" category="Social" class="category">
                                <i class="fa fa-users"></i> Social
                            </a>
                        </li>
                        <li>
                            <a href="#" category="Gadgets" class="category">
                                <i class="glyphicon glyphicon-headphones"></i> Gadgets
                            </a>
                        </li>
                        <li>
                            <a href="#" category="PC" class="category">
                                <i class="fa fa-desktop"></i> PC
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/ sidebar -->

        <div class="col-md-9 content">
            <div class="search-info margin-bottom-sm" style="margin-left:15px; margin-right:15px;">
                <div class="display-inline pull-left margin-top-sm">
                    <h4 id="result-header">Result for </h4>
                    <div id="result-info" style="color: #56585c;"></div>
                </div>
                <div class="display-inline pull-right margin-right-sm" style="margin-top: 20px">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default active">
                            <input type="radio" name="filter" id="filter-relevance" value="recip(abs(ms(NOW,created_time)),3.16e-11,1,1) scale(likes,0,1)" checked> Relevance
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="filter" id="filter-likes" value="likes"> Likes
                        </label>
                        <label class="btn btn-default">
                            <input type="radio" name="filter" id="filter-date" value="created_time"> Date
                        </label>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="result-cards">

            </div>

            <!--morebtn-->
            <nav aria-label="Page navigation">
                <center>
                    <ul class="pagination" id="pagination"></ul>
                </center>
            </nav>
        </div>
        <!--/List of Result-->
    </div>
</div>

<%--<script type="text/javascript">"use strict";$('#myAffix').affix({offset:{top:150,bottom:function(){return(this.bottom=$('.footer').outerHeight(true))}}})</script>--%>
</body>
</html>
