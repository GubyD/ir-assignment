package com.assignment.ir;

import com.assignment.ir.classification.FileManager;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by JCHANG007 on 16/2/17.
 */
public class FacebookManager {
    public static final String[] SOURCE_IDs = { "TechRadar", "t3mag", "techcrunch", "PCWorld", "Engadget" };

    private static final String ACCESS_TOKEN    = "CAAOZAaG4MjiwBALlAAZABBYNRj2ZBwZBlqYCTnPdX25nMNs6MymjJA1hVE7dgrjC43KLeWRsHcV7G7j3KB1uwHMmMdbbij4kN1RAifr2I3ZBFzwnG049wfLC9V3MA6QI76nNl2tF4ZBFZCnQA4ZAHGlMpOUrAgWISQ2RMBR6xER01IJh6FxF95FI";
    private static final String OPEN_GRAPH_URL  = "https://graph.facebook.com/v2.8/";

    private static final String REQUESTED_FIELD = "?fields=id,name,about,posts.limit(100){id,link,name,permalink_url,full_picture,created_time,description,message,likes.limit(0).summary(total_count)}";

    public static String get(String id) throws IOException {
        String url = getURL(id);
        return request(url);
    }

    public static String request(String url) throws IOException {
        HttpURLConnection conn = Utils.getConn(url);

        // get the content first
        String content = Utils.getContent(conn);

        // then disconnected it
        conn.disconnect();

        // return the content to the user
        return content;
    }

    public static void crawling() {
        for (String id:SOURCE_IDs) {
            try {
                //Raw Data
                String data = crawl(id);

                //
                Utils.writeTo(FileManager.getPath(FileManager.DIRECTORY_RAWDATA, id.concat(".json")), data.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String crawl(String id) {
        HashSet<String> hsTable = new HashSet<>();
        HashMap<String, Integer> hmTable = new HashMap<String, Integer>();
        try {
            int likes;
            String url, created_time, npost_id = "";
            boolean first_time = true, is_done = false;

            JSONArray  posts;
            JSONObject post, jobj;

            //Get the post_id of the last crawl data.
            String path = FileManager.getPath(FileManager.DIRECTORY_SOURCE_INFO, id);
            String post_id = Utils.readFirstLine(path);

            //Load new data.
            String content = get(id);

            //Parse the new raw data into JSONObject
            //Retrieved the posts from the crawled data
            JSONObject json = new JSONObject(content);
            json = json.getJSONObject("posts");

            //Container to store the necessary posts
            JSONArray mRawData = new JSONArray();

            while (true) {
                //To filter out unnecessary post from the crawl data.
                if (json.has("data")) {
                    posts = json.getJSONArray("data");

                    if (posts.length() != 0) {
                        if (first_time) {
                            first_time = false;

                            npost_id = posts.getJSONObject(0).getString("id");

                            if (npost_id.equals(post_id)) break;
                        }

                        //Add a source tag to identify later
                        for (int i = 0; i < posts.length(); i++) {
                            post = posts.getJSONObject(i);


                            if (post.has("name")) {
                                String header = post.getString("name");
                                header = header.replaceAll(FileManager.REGEX_URL, FileManager.SPACE);
                                header = header.replace("...", " ").trim();
                                //We don't want the normal post or timeline photos
                                //Thus, continue the loop (skip this post)
                                if (header.length() < 30 || header.equals("Timeline Photos") || header.contains("Netflix")) continue;

                                //Check duplicate post.
                                if (hsTable.contains(header)) continue;

                                String link = post.getString("link").toLowerCase();

                                if (!link.contains("news")    && !link.contains("article") && link.contains("features")   &&
                                    !link.contains("pcworld") && !link.contains("engt")    && !link.contains("techradar") &&
                                    !link.contains("buff")    && !link.contains("tcrn.ch")) continue;

                                //Check whether the post is being crawled before
                                created_time = post.getString("created_time");

                                if (post.getString("id").equals(post_id) ||
                                        created_time.contains("2016-01")) {
                                    is_done = true;
                                    break;
                                }

                                //Update the created time into solr datetime format.
                                created_time = created_time.replace("+0000", "Z");
                                post.put("created_time", created_time);

                                //Get the likes count from Facebook post
                                likes = post.getJSONObject("likes").getJSONObject("summary").getInt("total_count");
                                post.put("likes", likes);

                                //Add source that the post is retrieve
                                post.put("source", id);

                                mRawData.put(post);
                                hsTable.add(header);
                            }
                        }
                    } else break;
                }

                //Not done AND there is next page.
                if (!is_done && json.has("paging")) {
                    //Get the next page content.
                    url = json.getJSONObject("paging").getString("next");
                    content = request(url);

                    //Update json object for the next filter.
                    json = new JSONObject(content);
                } else { break; }
            }

            //Update the post id for the sources
            //To keep track of the data we retrieve
            Utils.writeTo(path, npost_id);

            System.out.println("Number of Post for "+ id +":"+ mRawData.length());
            //Return crawl data
            return mRawData.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

//    public static String crawl(String id) {
//        List<String> slist = new ArrayList<>();
//
//        try {
//            String url;
//            int counter = 0;
//
//            JSONArray  posts;
//
//            // the new data
//            String content = get(id);
//            JSONObject json = new JSONObject(content);
//
//            json = json.getJSONObject("posts");
//
//            while (true) {
//                if (json.has("data")) {
//                    posts = json.getJSONArray("data");
//
//                    if (posts.length() != 0) {
//                        String data = posts.toString();
//                        data.replace("+0000", "Z");
//                        data = data.substring(1, data.length()-1);
//
//                        slist.add(data);
//                    } else break;
//                }
//
//                if (counter <= 100 && json.has("paging")) {
//                    url = json.getJSONObject("paging").getString("next");
//                    content = request(url);
//
//                    json = new JSONObject(content);
//                } else {
//                    break;
//                }
//
//                counter++;
//            }
//        } catch (Exception e) {}
//
//        System.out.println(id+":"+slist.size());
//
//        return slist.toString();
//    }

    // for experiment purpose
    public static String manuallyCrawl(ServletContext ctx) {
        try {
            // maybe consider to crawl at the background
            for (String id: SOURCE_IDs) {
                String url;
                int counter = 0;

                JSONArray  posts;
                JSONObject post, jobj;

//                String path = ctx.getRealPath("/source_information/"+id);
//                String post_id = Utils.readFirstLine(path);
//
//                // get saved data
//                path = ctx.getRealPath("/rawdata/"+id+".json");
//                String content = Utils.read(path);
//                JSONArray  data = new JSONArray(content.equals("")?"[]":content);

                String path;
                JSONArray data = new JSONArray();

                // the new data
                String content = get(id);
                JSONObject json = new JSONObject(content);

                json = json.getJSONObject("posts");

                while (true) {
                    if (json.has("data")) {
                        posts = json.getJSONArray("data");

                        for (int i = 0; i < posts.length(); i++) {
                            post = posts.getJSONObject(i);
                            post.put("source", id);

                            jobj = new JSONObject();

                            jobj.put("id", post.getString("id"));
                            jobj.put("permalink_url_s", post.getString("permalink_url"));
                            jobj.put("created_time_dt", post.getString("created_time").replace("+0000", "Z"));

                            if (post.has("link")) {
                                jobj.put("link_s", post.getString("link"));
                            }

                            if (post.has("name")) {
                                jobj.put("name_s", post.getString("name"));
                            }

                            if (post.has("full_picture")) {
                                jobj.put("full_picture_s", post.getString("full_picture"));
                            }

                            if (post.has("description")) {
                                jobj.put("description_t", post.getString("description"));
                            }

                            if (post.has("message")) {
                                jobj.put("message_t", post.getString("message"));
                            }

                            data.put(jobj);
                        }
                    }

//                    // check whether the post we had store
//                    if (post_id != null && content.contains(post_id)) {
//                        break;
//                    }

                    if (counter <= 30 && json.has("paging")) {
                        url = json.getJSONObject("paging").getString("next");
                        content = request(url);

                        json = new JSONObject(content);
                    } else {
                        break;
                    }

                    counter++;
                }

//                // new post id
//                post_id = data.getJSONObject(0).getString("id");
//
//                // update the post id for the sources
//                // in order to keep track of the data we retrieve
//                path = ctx.getRealPath("/source_information/"+id);
//                Utils.writeTo(path, post_id);

                // get the path to store the rawdata
                // this part is for testing purpose
                path = ctx.getRealPath("/rawdata/"+id+".json");

                // write to file
                Utils.writeTo(path, data.toString());
                System.out.println(id+":"+data.length());
            }

            return "true";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    private static String getURL(String id) {
        return OPEN_GRAPH_URL + id + REQUESTED_FIELD +"&access_token="+ ACCESS_TOKEN;
    }
}
