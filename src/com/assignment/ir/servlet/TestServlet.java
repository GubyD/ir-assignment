package com.assignment.ir.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by JCHANG007 on 1/4/17.
 */
@WebServlet(name = "Test Servlet", urlPatterns = {"/Test"})
public class TestServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println(getServletContext().getRealPath("/WEB-INF"));
        File file = new File("test/cnet.json");
        System.out.println(file.getAbsolutePath());
//       String result = "";
//        try {
//            FileManager.ctx = getServletContext();
//
//            for (String facebookID: FacebookManager.SOURCE_IDs) {
//                String data = Utils.read(FileManager.getFile(FileManager.DIRECTORY_PREPROCESSED, facebookID.concat(".json")));
//                result = SolrManger.update(data);
//            }
//        } catch(Exception e) {
//            e.printStackTrace();
//        }
//
//        response.setContentType("text/plain");
//        PrintWriter out = response.getWriter();
//        out.println(result);
    }
}
