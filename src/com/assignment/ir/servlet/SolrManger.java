package com.assignment.ir.servlet;

import com.assignment.ir.Utils;
import com.assignment.ir.classification.FileManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by JCHANG007 on 2/2/17.
 */
@WebServlet(name = "SolrManger", urlPatterns = {"/Solr","/autocomplete","/spell","/search"})
public class SolrManger extends HttpServlet {
    private static final String SOLR_URL   = "http://localhost:8983/solr/techgeek/";
    private static final String SOLR_SPELL   = "spell";
    private static final String SOLR_SELECT  = "select";
    private static final String SOLR_SUGGEST = "suggest";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileManager.ctx = getServletContext();

        String sort, query, results = "";

        query = request.getParameter("q");

        switch (request.getServletPath()) {
            case "/autocomplete":
                solrQuery(SOLR_SUGGEST, query);
                break;
            case "/spell":
                solrQuery(SOLR_SPELL, query);
                break;
            case "/search":
                sort = request.getParameter("sort");

                if (sort == null || sort.equals("")) {
                    results = search(query);
                } else {
                    results = search(query, sort);
                }

                break;
            default:
                break;
        }

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");

        out.println(results);
        out.close();


//        FileManager.BASE_PATH = getServletContext().getRealPath("rawdata");
//        response.setContentType("text/plain");
//
//        String data =  "[\n" +
//                " {\"id\" : \"book8\",\n" +
//                "  \"title_txt_en\" : \"The Way of Kings\",\n" +
//                "  \"author_s\" : \"Brandon Sanderson\"\n" +
//                " }\n" +
//                "]";
//
////        String result = query("demo", "title_t:Kings", "author_s");
////        String result = FacebookManager.get("cnet");
//
//        try {
////            JSONObject json = new JSONObject(result);
////            String path = getServletContext().getRealPath("/rawdata/cnet.json");
////            Utils.writeTo(path, result);
//
////            FileManager.convertJSONtoARFF("t3mag.json");
////            FileManager.convertJSONtoARFF("Engadget.json");
////            FileManager.convertJSONtoARFF("PCWorld.json");
////            FileManager.convertJSONtoARFF("techcrunch.json");
////            FileManager.convertJSONtoARFF("TechRadar.json");
//
//
////            FacebookManager.crawling(getServletContext());
//
////            String msg = FacebookManager.crawling(getServletContext());
////            String msg = FacebookManager.crawl(getServletContext());
//            System.out.println(System.getProperty("os.name"));
//            System.out.println(FileManager.BASE_PATH);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        PrintWriter out = response.getWriter();
//        out.write("done");
////        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
////        rd.forward(request, response);
    }

    private String query(String collection, String q) throws ServletException, IOException {
        return query(collection, q, null);
    }

    private static String getSolrUrl(String reqHandler) {
        return SOLR_URL.concat(reqHandler);
    }

    public static String search(String q) throws ServletException, IOException {
        return search(q, "recip(abs(ms(NOW,created_time)),3.16e-11,1,1) scale(likes,0,1)");
    }

    public static String search(String q, String bf) throws ServletException, IOException {
        return search(q, bf, null);
    }

    public static String search(String q, String bf, String category) throws ServletException, IOException {
        String url = getSolrUrl(SOLR_SELECT);

        url = url.concat("?bf=".concat(bf));
        url = url.concat("&defType=edismax");
        url = url.concat("&indent=on&wt=json");
        url = url.concat("&q=".concat(q));

        if (category == null) {
            url = url.concat("&facet.field=category&facet=on");
        } else {
            url = url.concat("&fq=category:".concat(category));
        }

        url = URLEncoder.encode(url, "UTF-8");

        URL obj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

        return Utils.toString(conn.getInputStream());
    }

    public static String solrQuery(String reqHandler, String q) throws ServletException, IOException {
        String url = getSolrUrl(reqHandler);

        url = url.concat("?indent=on&wt=json");
        url = url.concat("&q=".concat(q));

        URL obj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

        return Utils.toString(conn.getInputStream());
    }

    public static String query(String collection, String q, String fl) throws ServletException, IOException {
        String url = SOLR_URL + collection + "/query";

        url += "?q=" + q;

        if (fl != null)
            url += "&fl=" + fl;

        URL obj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

        return Utils.toString(conn.getInputStream());
    }

    public static String update(String data) throws ServletException, IOException {
        String url =  getSolrUrl("update");

        url = url.concat("?commit=true");

        URL obj = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) obj.openConnection();

        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Content-Length", String.valueOf(data.length()));
        conn.setDoOutput(true);

        OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
        out.write(data);
        out.close();

        return Utils.toString(conn.getInputStream());
    }
}
