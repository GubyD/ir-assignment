package com.assignment.ir.servlet;

import com.assignment.ir.FacebookManager;
import com.assignment.ir.Utils;
import com.assignment.ir.classification.FileManager;
import com.assignment.ir.classification.WekaManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by JCHANG007 on 2/4/17.
 */
@WebServlet(name = "CrawlServlet", urlPatterns = {"/Crawl"})
public class CrawlServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileManager.ctx = getServletContext();

        //Crawling data from Facebook Open Graph API.
        FacebookManager.crawling();

        try {
            classifyCrawlData();
            uploadCrawlData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void classifyCrawlData() throws Exception {
        for (String facebookID: FacebookManager.SOURCE_IDs) {
            FileManager.convertJSONtoARFF(facebookID.concat(".json"));
            WekaManager.predict(facebookID.concat(".arff"), "classifier.model");
            WekaManager.update(facebookID);
        }

        System.out.println("Classify Crawl Data");
    }

    public static String uploadCrawlData() throws Exception {
        String result = "";
        for (String facebookID: FacebookManager.SOURCE_IDs) {
            String data = Utils.read(FileManager.getFile(FileManager.DIRECTORY_PREPROCESSED, facebookID.concat(".json")));
            result = SolrManger.update(data);
        }

        return result;
    }
}
