package com.assignment.ir.servlet;

import com.assignment.ir.classification.FileManager;
import com.assignment.ir.classification.WekaManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by JCHANG007 on 2/4/17.
 */
@WebServlet(name = "ClassificationServlet", urlPatterns = {"/Classification"})
public class ClassificationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileManager.ctx = getServletContext();

        try {
            //Generate the filter from the training set
            WekaManager.generateFilter();

            //Build classifier model
            WekaManager.buildClassifier();

            //Validate classifier model using the training set
            WekaManager.crossValidate("classifier.model", "dataset-training-filtered.arff");
        } catch(Exception e) {
            e.printStackTrace();
        }

    }
}
