package com.assignment.ir.classification;

import com.assignment.ir.Utils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.SMO;
import weka.classifiers.meta.Bagging;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.stemmers.SnowballStemmer;
import weka.core.tokenizers.WordTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

import java.util.Random;

/**
 * Created by JCHANG007 on 29/3/17.
 */
public class WekaManager {

    public static StringToWordVector generateFilter() throws Exception {
        String    arffFilePath   = FileManager.getPath("dataset-training.arff");
        Instances inputInstances = FileManager.loadARFF(arffFilePath, 0);

        //Initialize the StringToWordVectorFilter
        StringToWordVector filter = new StringToWordVector();

        filter.setInputFormat(inputInstances);
        filter.setTokenizer(new WordTokenizer());
        filter.setStemmer(new SnowballStemmer());
        filter.setDoNotOperateOnPerClassBasis(true);
        filter.setLowerCaseTokens(true);
        filter.setWordsToKeep(10000);
        filter.setMinTermFreq(2);
        filter.setStopwords(FileManager.getFile(FileManager.DIRECTORY_ARFF, "stop-words.txt"));

        //Filter the input instances
        Instances outputInstances = Filter.useFilter(inputInstances, filter);

        //Save the output as an ARFF file.
        FileManager.saveARFF(outputInstances, FileManager.getPath("dataset-training-filtered.arff"));

        //Serialize the filter
        SerializationHelper.write(FileManager.getPath(FileManager.DIRECTORY_MODEL, "string-to-word-vector.model"), filter);

        return filter;
    }

    public static Instances filter(String targetARFF) throws Exception {
        Instances inputInstances = FileManager.loadARFF(targetARFF, 0);
        StringToWordVector filter = loadFilter();

        return Filter.useFilter(inputInstances, filter);
    }

    public static StringToWordVector loadFilter() throws Exception {
        return (StringToWordVector) SerializationHelper.read(FileManager.getPath(FileManager.DIRECTORY_MODEL, "string-to-word-vector-filter.model"));
    }

    public static Classifier loadClassifier(String filename) throws Exception {
        return (Classifier) SerializationHelper.read(FileManager.getPath(FileManager.DIRECTORY_MODEL, filename));
    }

    public static void buildClassifier() throws Exception {
        //Load the filtered training dataset.
        String mTrainingSetPath = FileManager.getPath("dataset-training-filtered.arff");

        Instances instances = FileManager.loadARFF(mTrainingSetPath, 0);

        //Depending on which classifier we using
        //Set Bagging classifier.
        Bagging baggingClassifier = new Bagging();
        SMO SMOClassifier = new SMO();
        baggingClassifier.setClassifier(SMOClassifier);
        baggingClassifier.setBagSizePercent(300);
        baggingClassifier.setNumIterations(10);

        //Build the classifier.
        baggingClassifier.buildClassifier(instances);

        //Serialize the classifier.
        SerializationHelper.write(FileManager.getPath(FileManager.DIRECTORY_MODEL, "classifier.model"), baggingClassifier);
    }

    public static Evaluation crossValidate(String inClassifierFile, String inDataSetFile) throws Exception {
        //Convert to Full Path
        inClassifierFile = FileManager.getPath(FileManager.DIRECTORY_MODEL, inClassifierFile);
        inDataSetFile    = FileManager.getPath(inDataSetFile);

        //Load the dataset (train/test) as instances
        Instances mDataSet  = FileManager.loadARFF(inDataSetFile, 0);

        //Deserialize the pre-built classifier.
        Classifier mClassifier = (Classifier) SerializationHelper.read(inClassifierFile);

        //Evaluate the classifier.
        Evaluation eval = new Evaluation(mDataSet);
        eval.crossValidateModel(mClassifier, mDataSet, 10, new Random(1));

        //Print the evaluation result.
        System.out.println(eval.toSummaryString());
        System.out.println(eval.toClassDetailsString());
        System.out.println(eval.toMatrixString());

        return eval;
    }

    public static void predict(String unclassifyFile, String classifierFile) throws Exception {
        //Get Full Path
        unclassifyFile    = FileManager.getPath(FileManager.DIRECTORY_ARFF, unclassifyFile);
        String filterFile = FileManager.getPath(FileManager.DIRECTORY_MODEL, "string-to-word-vector.model");

        //Filter the un-classify data from string to word vector.
        Instances unclassifyData = filter(unclassifyFile, 0, filterFile);

        //Load un-classify data as instances to update the class value later on.
        Instances classifiedData = FileManager.loadARFF(unclassifyFile, 0);

        //Load the pre-built classifier model.
        Classifier classifier = loadClassifier(classifierFile);

        // label instances
        for (int i = 0; i < unclassifyData.numInstances(); i++) {
            double clsLabel = classifier.classifyInstance(unclassifyData.instance(i));
            classifiedData.instance(i).setClassValue(clsLabel);
        }

        // save labeled data
        FileManager.saveARFF(classifiedData, FileManager.getPath(FileManager.DIRECTORY_CLASSIFIED, FilenameUtils.getBaseName(unclassifyFile).concat(".arff")));
    }

    public static void update(String id) throws Exception {
        //Load the raw data
        String content = Utils.read(FileManager.getFile(FileManager.DIRECTORY_RAWDATA, id.concat(".json")));
        JSONArray data = new JSONArray(content);

        //Load the classified data as instances
        Instances classified = FileManager.loadARFF(FileManager.getPath(FileManager.DIRECTORY_CLASSIFIED, id.concat(".arff")), 0);

        String category;
        JSONObject post;

        for (int i = 0; i < data.length(); i++) {
            category = classified.instance(i).stringValue(0);
            post = data.getJSONObject(i);
            post.put("category", category);
        }

        Utils.writeTo(FileManager.getPath(FileManager.DIRECTORY_PREPROCESSED, id.concat(".json")), data.toString());
    }

    public static Instances filter(String arffFile, int classIndex, String filterFile) throws Exception {

        Instances inputInstances = FileManager.loadARFF(arffFile, classIndex);
        Instances outputInstances;

        //Set the filter.
        StringToWordVector filter = (StringToWordVector) SerializationHelper.read(filterFile);

        //Filter the input instances into the output ones.
        outputInstances = Filter.useFilter(inputInstances, filter);

        return outputInstances;
    }
}
