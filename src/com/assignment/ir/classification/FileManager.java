package com.assignment.ir.classification;

import com.assignment.ir.Utils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by JCHANG007 on 23/3/17.
 */
public class FileManager {
    public static ServletContext ctx;
    public static final String SPACE     = " ";
    public static final String SEPARATOR = "/";

    public static final String MAIN_DIRECTORY         = "techgeek/";
    public static final String DIRECTORY_RAWDATA      = "rawdata";
    public static final String DIRECTORY_ARFF         = "arff";
    public static final String DIRECTORY_MODEL        = "model";
    public static final String DIRECTORY_SOURCE_INFO  = "source_information";
    public static final String DIRECTORY_CLASSIFIED   = "classified";
    public static final String DIRECTORY_PREPROCESSED = "preprocessed";

    public static final String REGEX_URL              = "(https?://)?([\\da-z.-]+)\\.([a-z.]*)([/\\w.-]*)*/?";
    public static final String REGEX_SPACE            = "\\s+";
    public static final String REGEX_NON_LETTER_SPACE = "[^A-Za-z ]";

    public static String getPath(String directory, String filename) throws Exception {
        return getFile(directory, filename).getAbsolutePath();
    }

    public static String getPath(String filename) throws Exception {
        return getFile(filename).getAbsolutePath();
    }

    public static File getFile(String filename) throws Exception {
        return getFile(null, filename);
    }

    public static File getFile(String directory, String filename) throws Exception {
        directory = (directory == null) ? MAIN_DIRECTORY : MAIN_DIRECTORY + directory;
        String relativePath = directory + SEPARATOR + filename;

        File f = new File(relativePath);

        // if the directory is not exist
        if (!f.getParentFile().exists()) {
            f.getParentFile().mkdirs();
        }

        if (!f.exists()) {
            f.createNewFile();
        }

        return f;
    }

    public static void convertJSONtoARFF(String filename) throws Exception {
        HashSet<String> mHashTable = new HashSet<>();

        String content = Utils.read(getFile(DIRECTORY_RAWDATA, filename));
        String basename = FilenameUtils.getBaseName(filename);
        String title, message, data;

        StringBuilder sb = new StringBuilder();
        ArrayList<String> strArr = new ArrayList<>();

        sb.append("@relation ".concat(basename).concat(System.lineSeparator()));
        sb.append(System.lineSeparator());
        sb.append("@attribute @class@ ".concat("{Mobile,Computing,Gaming,Social,Gadgets,PC}").concat(System.lineSeparator()));

        JSONObject post;
        JSONArray  posts = new JSONArray(content);

        for (int i = 0; i < posts.length(); i++) {
            post = posts.getJSONObject(i);

            title = "";

            if (post.has("name")) {
                title = title.concat(post.getString("name") + SPACE);
            }

            /*
            if (post.has("description")) {
                message = message.concat(post.getString("description") + SPACE);
            }

            if (post.has("message")) {
                message.concat(post.getString("message") + SPACE);
            }
            */

            title = title
                        .replaceAll(REGEX_NON_LETTER_SPACE, " ")
                        .replaceAll(REGEX_SPACE, " ")
                        .trim();

            if (title.length() == 0) {
                title = "nil";
            }

            strArr.add(title);
            mHashTable.add(title);
        }

        message = String.join("','", mHashTable);
        sb.append("@attribute message {'".concat(message).concat("'}").concat(System.lineSeparator()));
        sb.append(System.lineSeparator());
        sb.append("@data".concat(System.lineSeparator()));

        data = String.join("','", strArr);
        data = data.replace(",", System.lineSeparator().concat("?,"));
        sb.append("?,'".concat(data).concat("'"));

        Utils.writeTo(getFile(DIRECTORY_ARFF, basename.concat(".arff")).getAbsolutePath(), sb.toString());
    }

    public static Instances loadARFF (String filename, int classIndex) throws Exception {

        ConverterUtils.DataSource source = new ConverterUtils.DataSource(filename);
        Instances inputInstances = source.getDataSet();
        if (inputInstances.classIndex() == -1)
            inputInstances.setClassIndex(classIndex);
        return inputInstances;

    }

    public static void saveARFF(Instances outputInstances, String filename) throws Exception {
        PrintWriter writer = new PrintWriter(new FileWriter(filename));
        writer.print(outputInstances);
        writer.close();
    }
}
