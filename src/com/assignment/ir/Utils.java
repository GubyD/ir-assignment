package com.assignment.ir;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by JCHANG007 on 2/2/17.
 */
public class Utils {
    public static String read(File file) {
        StringBuilder  sb = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));

            String s;
            while ((s = br.readLine()) != null) {
                sb.append(s+"\n");
            }

            br.close();
        } catch (IOException e) {}

        return sb.toString();
    }

    public static String read(String path) {
        return read(new File(path));
    }

    public static String toString(InputStream is) throws IOException {
        StringBuilder  sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String s;
        while ((s = br.readLine()) != null) {
            sb.append(s+"\n");
        }

        br.close();

        return sb.toString();
    }

    public static HttpURLConnection getConn(String url) throws IOException {
        URL obj = new URL(url);

        return (HttpURLConnection) obj.openConnection();
    }

    public static void writeTo(String path, String data) throws IOException {
        Files.write(Paths.get(path), data.getBytes());
    }

    public static String readFirstLine(String path) {
        Path p = Paths.get(path);

        try {
            List<String> strings = Files.readAllLines(p);

            if (!strings.isEmpty()) {
                return strings.get(0);
            }
        } catch (IOException e) {}

        return "";
    }

    public static String getContent(HttpURLConnection conn) throws IOException {
        return toString(conn.getInputStream());
    }
}
